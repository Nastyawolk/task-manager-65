package ru.t1.volkova.tm.endpoint;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.volkova.tm.api.endpoint.IProjectCollectionEndpoint;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

@RestController
@RequestMapping("/api/projects")
public class ProjectCollectionEndpointImpl implements IProjectCollectionEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping
    public @NotNull List<ProjectDTO> get() {
        return projectService.findAll();
    }

    @Override
    @PostMapping
    public void post(@RequestBody @NotNull List<ProjectDTO> projects) {
        projectService.saveAll(projects);
    }

    @Override
    @PutMapping
    public void put(@RequestBody @NotNull List<ProjectDTO> projects) {
        projectService.saveAll(projects);
    }

    @Override
    @DeleteMapping
    public void delete() {
        projectService.removeAll();
    }

}
