package ru.t1.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.volkova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.dto.model.TaskDTO;

@RestController
@RequestMapping("/api/task")
public class TaskEndpointImpl implements ITaskEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping("/{id}")
    public @Nullable TaskDTO get(@PathVariable("id") @NotNull String id) {
        return taskService.findById(id);
    }

    @Override
    @PostMapping
    public void post(@RequestBody @NotNull TaskDTO task) {
        taskService.create(task);
    }

    @Override
    @PutMapping
    public void put(@RequestBody @NotNull TaskDTO task) {
        taskService.update(task);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") @NotNull String id) {
        taskService.deleteById(id);
    }

}
