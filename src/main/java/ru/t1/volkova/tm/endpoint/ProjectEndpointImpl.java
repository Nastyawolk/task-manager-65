package ru.t1.volkova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.volkova.tm.api.service.IProjectService;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

@RestController
@RequestMapping("/api/project")
public class ProjectEndpointImpl implements ru.t1.volkova.tm.api.endpoint.IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/{id}")
    public @Nullable ProjectDTO get(@PathVariable("id") @NotNull String id) {
        return projectService.findById(id);
    }

    @Override
    @PostMapping
    public void post(@RequestBody @NotNull ProjectDTO project) {
        projectService.create(project);
    }

    @Override
    @PutMapping
    public void put(@RequestBody @NotNull ProjectDTO project) {
        projectService.update(project);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") @NotNull String id) {
        projectService.deleteById(id);
    }

}
