package ru.t1.volkova.tm.endpoint;

import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.t1.volkova.tm.api.endpoint.ITaskCollectionEndpoint;
import ru.t1.volkova.tm.api.service.ITaskService;
import ru.t1.volkova.tm.dto.model.TaskDTO;

@RestController
@RequestMapping("/api/tasks")
public class TaskCollectionEndpointImpl implements ITaskCollectionEndpoint {

    @Autowired
    private ITaskService taskService;

    @Override
    @GetMapping()
    public @NotNull List<TaskDTO> get() {
        return taskService.findAll();
    }

    @Override
    @PostMapping
    public void post(@RequestBody @NotNull List<TaskDTO> tasks) {
        taskService.saveAll(tasks);
    }

    @Override
    @PutMapping
    public void put(@RequestBody @NotNull List<TaskDTO> tasks) {
        taskService.saveAll(tasks);
    }

    @Override
    @DeleteMapping()
    public void delete() {
        taskService.removeAll();
    }

}
