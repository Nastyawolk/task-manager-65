package ru.t1.volkova.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.volkova.tm")
public class ApplicationConfiguration {

}
