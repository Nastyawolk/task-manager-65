package ru.t1.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.volkova.tm.dto.model.TaskDTO;

public interface ITaskEndpoint {

    @GetMapping("/{id}")
    @Nullable
    TaskDTO get(@PathVariable("id") @NotNull String id);

    @PostMapping
    void post(@RequestBody @NotNull TaskDTO task);

    @PutMapping
    void put(@RequestBody @NotNull TaskDTO task);

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") @NotNull String id);

}
