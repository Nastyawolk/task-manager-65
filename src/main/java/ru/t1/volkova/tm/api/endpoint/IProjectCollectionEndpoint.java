package ru.t1.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectCollectionEndpoint {

    @GetMapping
    @NotNull
    List<ProjectDTO> get();

    @PostMapping
    void post(@RequestBody @NotNull List<ProjectDTO> projects);

    @PutMapping
    void put(@RequestBody @NotNull List<ProjectDTO> projects);

    @DeleteMapping
    void delete();

}
