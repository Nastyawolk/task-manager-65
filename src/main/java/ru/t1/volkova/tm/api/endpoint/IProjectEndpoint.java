package ru.t1.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.volkova.tm.dto.model.ProjectDTO;

public interface IProjectEndpoint {

    @GetMapping("/{id}")
    @Nullable
    ProjectDTO get(@PathVariable("id") @NotNull String id);

    @PostMapping
    void post(@RequestBody @NotNull ProjectDTO project);

    @PutMapping
    void put(@RequestBody @NotNull ProjectDTO project);

    @DeleteMapping("/{id}")
    void delete(@PathVariable("id") @NotNull String id);

}
