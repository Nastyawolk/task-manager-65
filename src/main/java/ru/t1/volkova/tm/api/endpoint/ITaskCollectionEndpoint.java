package ru.t1.volkova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.web.bind.annotation.*;
import ru.t1.volkova.tm.dto.model.TaskDTO;

import java.util.List;

public interface ITaskCollectionEndpoint {

    @GetMapping()
    @NotNull
    List<TaskDTO> get();

    @PostMapping
    void post(@RequestBody @NotNull List<TaskDTO> tasks);

    @PutMapping
    void put(@RequestBody @NotNull List<TaskDTO> tasks);

    @DeleteMapping()
    void delete();

}
